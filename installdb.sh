#!/usr/bin/env bash
#
# Install InfluxDB in a Docker container
#

: ${DBDIR=/data/influxdb}
: ${DBNAME=covis}

IMAGE="influxdb:alpine"

user="$1"
password="$2"

if [[ -z $user ]] || [[ -z $password ]]; then
    echo "Usage: $(basename $0) user password"
    exit 1
fi

docker pull $IMAGE
mkdir -p $DBDIR
docker run --rm \
  -e INFLUXDB_DB=$DBNAME \
  -e INFLUXDB_USER="$user" \
  -e INFLUXDB_USER_PASSWORD="$password" \
  -v $DBDIR:/var/lib/influxdb $IMAGE /init-influxdb.sh

#!/usr/bin/env bash
#
# Wait for a TCP service to start
#
host="$1"
port="$2"

if [[ -z $host ]] || [[ -z $port ]]; then
    exit 1
fi

while true; do
    /bin/nc -z -w 2 "$host" "$port" && break
done

exit 0

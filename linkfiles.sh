#!/usr/bin/env bash
#
# Link the COVIS data archives into a date-based directory tree to make
# them easier to browse.
#

: ${TOPDIR=/data/BROWSE}

# Create a date/time based directory tree from an archive timestamp. Given a
# timestamp of the form YYYYMMDDThhmmss, the resulting directory tree is
# YYYY/MM/DD
create_dirtree ()
{
    local tstamp d
    tstamp="$1"
    d=$(date -u -d "${tstamp:0:8}" +'%Y/%j')
    echo "$d"
}

process_archive ()
{
    local tstamp dirtree
    filename="${1##*/}"
    tstamp="$(cut -f2 -d- <<< ${filename})"
    dirtree=$(create_dirtree $tstamp)
    mkdir -p "$TOPDIR/$dirtree"
    [[ -d "$TOPDIR/$dirtree" ]] && ln "$1" "$TOPDIR/$dirtree/" 2> /dev/null
}

while read pathname; do
    process_archive "$pathname"
done < <(find /data/COVIS -name 'COVIS-*.tar.gz' -print)

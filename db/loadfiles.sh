#!/usr/bin/env bash
#
# Load a series of newline-delimited JSON into the COVIS database.
#

declare -A TOPICS
TOPICS=(
    ["therm"]="covis.therm"
    ["orientation"]="covis.move"
    ["sweep"]="covis.sweep"
)

: ${NETWORK=covisdb_dbnet}
: ${DATADIR=.}

for d in $(find $DATADIR -type d -print); do
    pushd $d
    files=(*.json)
    args=()
    for f in "${files[@]}"; do
        class="${f%-*}"
        topic="${TOPICS[$class]}"
        [[ -n $topic ]] && [[ -s $f ]] && args+=( "/data/$f,$topic" )
    done
    if ((${#args[@]} != 0)); then
        docker run --rm -it \
               --network $NETWORK \
               -v "$PWD:/data" \
               -e "COVISDB_DBURL=http://db:8086" \
               -e "COVISDB_DBCREDS=sysop:reson" \
               -e "COVISDB_DBNAME=covis" \
               covisdb:latest load "${args[@]}"
    fi
    popd
done